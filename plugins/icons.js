import { library, dom } from '@fortawesome/fontawesome-svg-core'
import {
    fas,
    faStopwatch,
    faClock,
    faUtensils,
    faUtensilSpoon,
    faChevronLeft
} from '@fortawesome/free-solid-svg-icons'
library.add(
    fas,
    faStopwatch,
    faClock,
    faUtensils,
    faUtensilSpoon,
    faChevronLeft
);
dom.watch();